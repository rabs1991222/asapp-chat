import React from 'react';
import { useState, useEffect } from 'react';
// import SingleListItem from './single-list-component';
import { For } from 'react-loops'
import './user-header.css'

export default function UserHeader({currentUser}) {
    // debugger;
	return (
        <div className="user-header-continer">
            <div className="user-img">
                <img src={require(`../../assets/${currentUser.img}`)} />
            </div>

            <div className="user-name">
                {currentUser.username}
            </div>
        </div>
    );
};

