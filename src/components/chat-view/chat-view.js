import React from 'react';


import HeaderBar from '../header-bar-component/header-bar-component';
import ChatContainer from '../chat-container-component/chat-container';
import MessageBox from '../message-box-component/message-box';

import './chat-view.css';
import { useState, useEffect } from 'react';
import useGlobal from "../../store";
import ChatList from '../chat-list-component/chat-list'
import UserHeader from '../user-header-component/user-header'

const ChatView = (props) => {
	// debugger
	const [currentUser, setCurrentUser] = useState(props.currentUser);

	const [chatUser, setChatUser] = useState(props.chatUser);
	const [globalState, globalActions] = useGlobal();

	
	useEffect(()=>{
		globalActions.setUsers({currentUser: currentUser, chatUser: chatUser});
		// console.log('cambio');
	}, [currentUser]);

	
	
	return (
		<div className="main-container">
			
			
			<div className="list-chat-container">
				<div className="list-chats">
					<UserHeader currentUser={currentUser} />
					<ChatList  chatUser={chatUser} />
				</div>
			</div>
			
			<div className="body-container">
				<HeaderBar chatUser={chatUser} currentUser={currentUser} />
				<ChatContainer  currentUser={currentUser}/>
				<MessageBox currentUser={currentUser} />
			</div>
		</div>
	);
};

export default ChatView;
