import React from 'react';
import './header-bar.css';
import { useState, useEffect } from 'react';
import useGlobal from "../../store";

const HeaderBar = ({chatUser, currentUser}) => {
    const [status, setstatus] = useState('Online');
    const [globalState, globalActions] = useGlobal();
    const [isTyping, setIstyping] = useState('');
    const [currentId, setCurrentId] = useState(currentUser.id);
    const [isOnline, seIsOnline] = useState('online');

    useEffect(()=>{
        let isTyping = globalState[currentId].chatUser.isTyping;
        if (isTyping){
            setIstyping("is typing...")
        } else {
            setIstyping("");
        }
    }, [globalState[currentId].chatUser.isTyping])
  
     

    return (
        <div className="header-bar">
            <div className="header-img">
                <img src={require(`../../assets/${chatUser.img}`)} />
            </div>
            <div className="header-info">
                <div className="header-user">
                    <div className="header-name">
                        {chatUser.name}
                    </div>
                    
                </div>
                <div className="header-info-text">
                    <div className="online">
                        {status}
                    </div>
                    
                    <div className="is-typing">
                        {isTyping}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HeaderBar;
