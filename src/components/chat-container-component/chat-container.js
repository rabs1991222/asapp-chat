import React , { useState, useEffect } from 'react';
import './chat-container.css';
import MessageElement from '../message-element-component/message-element';
import useGlobal from "../../store";
import { For } from 'react-loops'


const ChatContainer = ({currentUser}) => {

    const [globalState, globalActions] = useGlobal();
   

	return (
        <div className="chat-component" id="chat-component">
            <For of={globalState.conversationArray} as={(el, { isLast }) =>
               <MessageElement messageItem={el} currentId={currentUser.id} id />
            }/>
            
        </div>
    );
};

export default ChatContainer;
