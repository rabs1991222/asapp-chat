import React from 'react';
import './chat-list.css';
import { useState, useEffect } from 'react';

export default function ChatList({chatUser}) {
    const [isOnline, seIsOnline] = useState('online');

    // useEffect(()=>{
    //     let isOnline;
    //     if (chatUser.status) {
    //         isOnline = "online";
    //     } else {
    //         isOnline = "offline"
    //     }
    //     seIsOnline(isOnline)
    // }, [chatUser])

	return (
        <div className="chat-list-container">
            <div className="chat-list-title">
                CHATS
            </div>
            <div className="chat-list-contact">
                <span className={`${isOnline}`}></span>
                <div className="chat-list-user-name">
                    {chatUser.name}
                </div>
            </div>
        </div>
    );
};

