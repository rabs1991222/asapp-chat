import { renderTime } from "../utils";

export const addToChat = (store, msg) => {
    store.state.messages.push(msg);
    const messages = store.state.messages;
    store.setState({ messages });
};



export const isTyping = (store, isTyping, userInfo) => {
    store.state[userInfo.id].currentUser.isTyping  = isTyping;
    const currentUser = store.state[userInfo.id];
    store.setState({ [userInfo.id]:currentUser });
};


export const setUsers = (store, users) => {
    const id = users.currentUser.id;
    const currentUser = users.currentUser;
    const chatUser = users.chatUser;
    store.state[id] = {currentUser: currentUser, chatUser:chatUser};
    let user = store.state[id];
    store.setState({ [id] : user });
};

export const addMessaje = (store, msg, userId) => {

    const currentTime = renderTime();
    let hide = false;
    let user = store.state[userId];
    let conversationArray = store.state.conversationArray;
    let lastPosition = conversationArray.length -1;


    if (conversationArray.length > 0) {
        const lastMessageUserId = conversationArray[lastPosition].userId;
        hide = lastMessageUserId == userId;
    }

    let id =  Math.floor(Math.random() * 20) + Math.floor(Math.random() * 20);

    let msgObj = {
        id:id,
        userId: userId,
        username:user.currentUser.username, 
        img: store.state[userId].currentUser.img,
        time: currentTime,
        message:msg,
        hide:hide,
        sent : true,
        recieved:false
    }
    user.currentUser.messages.push(msgObj);
    conversationArray.push(msgObj)

    store.setState({[userId]:user, conversationArray:conversationArray}); 
}

export const setRevicievedMessage =(store, userId, msgId)=>{

    // let chatUserId = store.state[userId].chatUser.id;

    let conversationArray = store.state.conversationArray;

    for (let msg of conversationArray) {
        if (msg.id == msgId) {
            msg.recieved = true;
        }
    }
    // conver[msgId].recieved = true;
    store.setState({conversationArray:conversationArray});

}


// mg="img.png" message="hola como estas?" time="7:30" name="nombre"