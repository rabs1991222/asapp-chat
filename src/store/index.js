import React from "react";
import useGlobalHook from "use-global-hook";

import * as actions from "../actions";
const userObject = {
    currentUser: {},
    chatUser: {}
}
const initialState = {
    userA: userObject,
    userB: userObject,
    conversationArray : []
};

const useGlobal = useGlobalHook(React, initialState, actions);

export default useGlobal;
